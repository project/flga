## Face Login with GAuth module for Drupal
### INTRODUCTION
flga is a base module for providing face login along with google authenticator
for your Drupal site. As a base module, flga provides login facilty with your
face, to be more secure we have integrated google authentication support for
it. The base code for face login is taken as example from 
[ZOOCHA](https://www.zoocha.com/news/user-login-drupal-8-aws-rekognition) code
### REQUIREMENTS
This module is based on AWS REKOGNITION API so It Needs 'aws php sdk'.
````
composer require aws/aws-php-sdk
````
For Google Authenticator it needs:
````
composer require pragmarx/google2fa
````
For Qr Code generation:
````
composer require bacon/bacon-qr-code
````

Read more about the features and use of flga at its Drupal.org project page at
https://drupal.org/project/flga

### INSTALLATION

FLGA module can be installed like other Drupal modules by placing this directory
in the Drupal file system (for example, under modules/) and enabling on
the Drupal modules page.

### CONFIGURATION

FLGA can be configured on your Drupal site at Administration - Configuration -
SYSTEM - Face login Configuration. Add AWS credentials along with settings.
