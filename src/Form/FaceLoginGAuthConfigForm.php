<?php

namespace Drupal\face_login_gauth\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FaceLoginGAuthConfigForm.
 */
class FaceLoginGAuthConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'face_login_gauth.facelogingauthconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'face_login_g_auth_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('face_login_gauth.facelogingauthconfig');
    $form['region'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Region'),
      '#description' => $this->t('AWS Region'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('region'),
    ];
    $form['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#description' => $this->t('APP Version'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('version'),
    ];
    $form['aws_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Key'),
      '#description' => $this->t('AWS Account Key.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('aws_key'),
    ];
    $form['aws_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AWS Secret'),
      '#description' => $this->t('AWS Secret'),
      '#default_value' => $config->get('aws_secret'),
    ];
    $form['threshhold'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Image match threshhold'),
      '#description' => $this->t('threshhold'),
      '#default_value' => $config->get('threshhold'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('face_login_gauth.facelogingauthconfig')
      ->set('region', $form_state->getValue('region'))
      ->set('version', $form_state->getValue('version'))
      ->set('aws_key', $form_state->getValue('aws_key'))
      ->set('aws_secret', $form_state->getValue('aws_secret'))
      ->set('threshhold', $form_state->getValue('threshhold'))
      ->save();
  }

}
