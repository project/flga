<?php

namespace Drupal\face_login_gauth\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\face_login_gauth\FaceAuthSecretTrait;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class FaceLoginQrCodeDecoratorController.
 */
class FaceLoginQrCodeDecoratorController extends ControllerBase {

  use FaceAuthSecretTrait;

  /**
   * Symfony\Component\HttpFoundation\RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Drupal\user\UserDataInterface; definition.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * Constructs a new FaceLoginQrCodeGeneratorController object.
   */
  public function __construct(RequestStack $request_stack, AccountProxy $currentUser, UserDataInterface $userData) {
    $this->requestStack = $request_stack;
    $this->currentUser = $currentUser;
    $this->userData = $userData;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('current_user'),
      $container->get('user.data')
    );
  }

  /**
   * Decorator for QR code.
   *
   * @return array
   *   Qr code Image.
   */
  public function getQrCode() {
    if ($this->getUserData('face_login_gauth', 'secret', $this->currentUser->id(), $this->userData)) {
      $this->deleteUserData('face_login_gauth', 'secret', $this->currentUser->id(), $this->userData);
      $url = Url::fromRoute('face_login_gauth.face_login_qr_code_generator_controller_get_qr_code',
        ['user' => $this->currentUser->id()], ['query' => ['regenerate' => TRUE]]);
      $link = [
        '#type' => 'link',
        '#url' => $url,
        '#title' => $this->t('Click here to rendered Qr code image to scan.'),
      ];
      return $link;
    }
    if ($this->checkIfRegenerateImage() || empty($this->getUserData('face_login_gauth', 'secret', $this->currentUser->id(), $this->userData))) {
      $qrCodeImage = [
        '#theme' => 'image',
        '#uri' => Url::fromRoute('face_login_gauth.face_login_qr_code_generator_controller_get', ['user' => $this->currentUser->id()])->toString(),
      ];
      return [
        '#theme' => 'qr_code',
        '#qr_code' => $qrCodeImage,
      ];
    }
  }

  /**
   * Check if renegerated image requested.
   *
   * @return mixed
   *   Will return true or null.
   */
  private function checkIfRegenerateImage() {
    return $this->requestStack->getCurrentRequest()->get('regenerate');
  }

}
